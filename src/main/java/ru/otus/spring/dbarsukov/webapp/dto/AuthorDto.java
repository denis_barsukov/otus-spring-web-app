package ru.otus.spring.dbarsukov.webapp.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthorDto {
    private Long id;
    private String name;
    private String surname;
}
