package ru.otus.spring.dbarsukov.webapp.util;

import org.springframework.stereotype.Component;
import ru.otus.spring.dbarsukov.webapp.dto.AuthorDto;
import ru.otus.spring.dbarsukov.webapp.entity.Author;

@Component
public class AuthorConverter {

    public Author toEntity(AuthorDto dto) {
        return new Author(dto.getId(), dto.getName(), dto.getSurname());
    }
}
