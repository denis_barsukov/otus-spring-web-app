package ru.otus.spring.dbarsukov.webapp.repository;

import org.springframework.data.repository.CrudRepository;
import ru.otus.spring.dbarsukov.webapp.entity.Author;

import java.util.List;

public interface AuthorRepository extends CrudRepository<Author, Long> {
    List<Author> findAll();
}