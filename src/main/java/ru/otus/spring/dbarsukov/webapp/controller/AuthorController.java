package ru.otus.spring.dbarsukov.webapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.otus.spring.dbarsukov.webapp.dto.AuthorDto;
import ru.otus.spring.dbarsukov.webapp.exception.NotFoundException;
import ru.otus.spring.dbarsukov.webapp.service.BookStoreService;
import ru.otus.spring.dbarsukov.webapp.util.AuthorConverter;

@Controller
@RequiredArgsConstructor
public class AuthorController {

    private final BookStoreService bookStoreService;
    private final AuthorConverter authorConverter;

    @GetMapping("/authors")
    public String listAuthors(Model model) {
        model.addAttribute("authors", bookStoreService.getAllAuthors());
        return "listAuthor";
    }

    @GetMapping("/author")
    public String listOneAuthorById(@RequestParam("id") long id, Model model) {
        model.addAttribute("authors", bookStoreService.getAuthorById(id)
                .orElseThrow(NotFoundException::new));
        return "listAuthor";
    }

    @GetMapping("/author/edit")
    public String edithAuthorForm(@RequestParam("id") long id, Model model) {
        model.addAttribute("author", bookStoreService.getAuthorById(id)
                .orElseThrow(NotFoundException::new));
        return "editAuthor";
    }

    @PostMapping("/author/edit")
    public ModelAndView updateAuthor(@ModelAttribute AuthorDto dto) {
        bookStoreService.saveAuthor(authorConverter.toEntity(dto));
        return new ModelAndView("redirect:/authors");
    }

    @GetMapping(value = "/author/new")
    public String showNewAuthorForm(@ModelAttribute AuthorDto dto, Model model) {
        model.addAttribute("author", bookStoreService.getEmptyAuthor());
        return "editAuthor";
    }

    @PostMapping(value = "/author/new")
    public ModelAndView addAuthor(@ModelAttribute AuthorDto dto) {
        bookStoreService.saveAuthor(authorConverter.toEntity(dto));
        return new ModelAndView("redirect:/authors");
    }

    @GetMapping("/author/delete")
    public ModelAndView deleteAuthor(@RequestParam("id") long id) {
        bookStoreService.deleteAuthorById(id);
        return new ModelAndView("redirect:/authors");
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<String> handleMissingAuthor(NotFoundException ex) {
        return ResponseEntity.badRequest().body("Author not found");
    }
}
