package ru.otus.spring.dbarsukov.webapp.service;

import ru.otus.spring.dbarsukov.webapp.entity.Author;

import java.util.List;
import java.util.Optional;

public interface BookStoreService {
    List<Author> getAllAuthors();
    Optional<Author> getAuthorById(Long id);
    void saveAuthor(Author author);
    void deleteAuthorById(long id);
    Author getEmptyAuthor();
}
