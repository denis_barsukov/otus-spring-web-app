drop table if exists authors;

create table authors(
    id        bigserial primary key
  , name      varchar(255) not null
  , surname   varchar(255) not null
  , unique (name, surname)
);
