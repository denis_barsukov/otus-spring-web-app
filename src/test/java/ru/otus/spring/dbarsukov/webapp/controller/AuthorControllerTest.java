package ru.otus.spring.dbarsukov.webapp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.otus.spring.dbarsukov.webapp.entity.Author;
import ru.otus.spring.dbarsukov.webapp.exception.NotFoundException;
import ru.otus.spring.dbarsukov.webapp.service.BookStoreService;
import ru.otus.spring.dbarsukov.webapp.util.AuthorConverter;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willDoNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AuthorController.class)
class AuthorControllerTest {

    @Autowired
    MockMvc mvc;

    @MockBean
    AuthorConverter converter;

    @MockBean
    BookStoreService service;

    @Test
    void listAuthors() throws Exception{
        given(service.getAllAuthors())
                .willReturn(Arrays.asList(
                        new Author(1L, "Jules", "Verne"),
                        new Author(2L, "Ray", "Bradbury")));

        mvc.perform(get("/authors"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Jules")))
                .andExpect(content().string(containsString("Verne")))
                .andExpect(content().string(containsString("Ray")))
                .andExpect(content().string(containsString("Bradbury")));

        then(service).should().getAllAuthors();
    }

    @Test
    void viewAuthor() throws Exception {
        given(service.getAuthorById(1L))
                .willReturn(Optional.of(
                        new Author(1L, "Jules", "Verne")));

        mvc.perform(get("/author?id=1"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Jules")))
                .andExpect(content().string(containsString("Verne")));

        then(service).should().getAuthorById(1L);
    }

    @Test
    void updateAuthor() throws Exception {
        willDoNothing().given(service).saveAuthor(new Author(1L, "foo", "bar"));

        mvc.perform(post("/author/edit")
                .content("id=1&name=foo&surname=bar")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/authors"));
    }

    @Test
    void addAuthor() throws Exception {
        willDoNothing().given(service).saveAuthor(new Author(1L, "foo", "bar"));

        mvc.perform(post("/author/edit")
                .content("name=foo&surname=bar")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/authors"));
    }

    @Test
    void deleteAuthor() throws Exception {
        willDoNothing().given(service).deleteAuthorById(1L);

        mvc.perform(get("/author/delete?id=1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/authors"));
    }

    @Test
    void handleMissingAuthor() throws Exception {
        given(service.getAuthorById(Long.MAX_VALUE))
                .willThrow(NotFoundException.class);

        mvc.perform(get("/author?id=9223372036854775807"))
                .andExpect(status().is4xxClientError());

        then(service).should().getAuthorById(Long.MAX_VALUE);
    }
}